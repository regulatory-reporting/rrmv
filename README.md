# The Regulatory Reporting Metadata Vocabulary
 
## Context

Regulatory reporting is an interesting field of experimentation for digital-ready policymaking activities, as: 

* it is an horizontal process present across various policy domains; 
* it has a strong data angle; 

Additionally, it is characterized by the following challenges: 

* European SMEs are struggling with complying with multiple obligations; 
* public administrations often have a non-negligible implementation cost; 
* data and obligations set in legislation are often siloed in nature per domain and per topic, causing the absence of an overarching view of the burden imposed on specific actors; 

The existing systems used for managing regulatory reporting obligations are not interoperable, leading to inefficiencies and difficulties in managing reporting requirements. Policy officers, who are responsible for setting and monitoring such obligations, struggle due to a lack of a comprehensive view.  

To address these challenges, the SORTIS project developed an ontology, called Regulatory Reporting Metadata Vocabulary, involving Professor Monica Palmirani from the University of Bologna. Such ontology is expected to provide a foundational solution for applications dealing with regulatory reporting. This initiative aims to support setting up more efficient reporting obligations and potentially supports their implementation in the future. 

This ontology was developed in the context of the SORTIS project (Study On Regulatory reporTIng Standards) coordinated by the Digital-ready Policymaking Team and tthe SEMIC Team of the Interoperability and Digital Government Unit of the European Commission, DIGIT.B2. 

## An introduction to the RRMV ontology

The main goal of the project is to model legal ‘obligations’, now called ‘requests’, present European legislation to periodically report on some particular situation. The legislation also changes over time so there is a need to update this according to the modifications. 

For example, below it is possible to see an article of a regulation that expresses a request for an agent to report to other agents every five years.

![Art. 54 of the Regulation (EU) 2016/1011 on indices used as benchmarks in financial instruments and financial contracts](./img/example.png) 

## What is a Request?

A Request is a linguistic mandate addressed from some agents (addressers) to other agents (addressees) to perform one or more Actions in a given Time. 

An example of Request is the following.

Regulation (EU) 2016/1011 on indices used as benchmarks in financial instruments and financial contracts

>Art. 54
>2. Review the evolution of international principles applicable to benchmarks and of legal frameworks and supervisory practices in third countries concerning the provision of benchmarks and report to the European Parliament and to the Council every five years after 1 January 2018. That report shall assess in particular whether there is a need to amend this Regulation and shall be accompanied by a legislative proposal, if appropriate.

## Anatomy of a Request

By considering Requests as extensions of eli:Expression, they gain all ELI characteristics and properties starting with Uniform Resource Identifiers that make them univocally identifiable and referenceable.

Request references and cites the document it is part of. Its URI is more detailed compared to the Expression's URI, signifying its nature as a subset of the Expression. 

The ontology can be used to construct a network that traces the evolution of requests over time, considering legal modifications and the changes in the requirements needed to fulfill the request. 

It links requests across legal texts at different points in time, allowing users to access the appropriate version for their inquiry. 

Each Request may produce an Action, which can be interconnected to other actions. Actions can relate to each other semantically (isRelatedTo) or sequentially, denoted by the 'hasNext' property. Actions may carry annotations or statuses and involve an AgentRole and ActionResult over a specified PeriodOfTime, as well as a topic. Additionally, each Action related to a Request may have a variety of action statuses like completed, deleted, or overridden. 

A Request involve Agent(s) that should performe Action(s).
Action(s) are events that produce ActionResult(s) in a given time.  

![Action(s) are events that produce ActionResult(s) in a given time.](./img/action.png)


The Action(s) could be repetitive, for this reason we have Frequency.  

![Each Agent plays a Role in a given time.  ](./img/agent.png)

Each Request has Topic(s).
Each Agent plays a Role in a given time.  

## Competency questions
 
The RRMV ontology design was guided by the following competency questions:
* When is the next Action expected with an ActionResult, that is the report, related to a particular Concept? 
* What Action has a Status of “suspended” related to that Concept? 
* Who is the AgentRole, that is the addressee, of the Action within a Frequency of two to five years?


# Acknowledgments

* Monica Palmirani - Full Professor, Universita' di Bologna - ALMA-AI
* Andrea Nuzzolese - CNR-ISTC
* Cecile Guasch - External Service Provider, DG DIGIT.B2 - Digital-ready policymaking
* Alessio Nardin - External Service Provider, DG DIGIT.B2 - Digital-ready policymaking



