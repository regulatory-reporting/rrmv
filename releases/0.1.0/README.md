Release 0.1.0 - Internal review

The rrmv-cleaned.owl file is the target version 0.1.0.
It is a mannually cleaned version from the rrmv-orig.ttl one.
In particular it does not contain any codelist instances (moved to another file) and it does contain the correct temporal properties and classes using owl-time.

The rrmv-codes.ttl file contains the codes that were in the original VocBench content.
They are now encoded according to SKOS. 

The rrrmv-orig.ttl is current data in VocBench.

Steps to do create a final release:
  After the validation and adoption of the rrmv-cleaned.owl as the 0.1.0 version, the VocBench instance should be replaced with this content, 
  And the file rrmv-cleaned.owl should be renamed to rrmv.owl. The rrmv-orig.ttl can be removed then.
  
